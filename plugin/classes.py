#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" Module classes: This module contains extra classes to register 
Sublime Text menu commands. 
"""

import os
import sublime, sublime_plugin

class ExportAsTxtCommand(sublime_plugin.ApplicationCommand):
    """
    This class sublcasses the TextCommand class to register 
    a new command for exporting the current ZScript to a .txt file.    
    """

    def run(self, path=None):
        """
        This is called when the command is run.
        """

        sublime.status_message('Exporting current ZScript to .txt...')

        # Grab active view
        view = sublime.Window.active_view(sublime.active_window())

        # Get current file in view and write it as .txt file
        currentView = view.file_name()

        # Check for valid filename extension
        if os.path.splitext(currentView)[-1] == '.zs':

            data = open(currentView, 'r').read()

            writePath = os.path.splitext(currentView)[0] + '.txt'

        else:
            sublime.error_message('This is not a valid ZScript file!!!\n{0}'
                .format(currentView))
            return

        try: 
            writeFile = open(writePath, 'w')
            writeFile.write(data)

            writeFile.close()

            sublime.status_message('Successfully saved as: {0}!'.format(writePath))

        except IOError:
            sublime.error_message('Unable to write to: {0}!!!'.format(writePath))
            raise IOError

