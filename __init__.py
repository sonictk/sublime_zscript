#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" Module __init__.py """

import sys, os

# Add current directory to PYTHONPATH
sys.path.append(os.path.dirname(os.path.abspath(__file__)))

from ZScript.plugin.classes import *

