# README #

This package is a syntax highlighter for ZBrush ZScript syntax.

 Written for Sublime Text 3.


## Requirements ##

Sublime Text 3


## Installation ##

This plugin can be installed from Package Control directly within Sublime Text. This is the recommended method for end-users.

https://packagecontrol.io/packages/ZScript

#### Manual installation ####

If you are intending to develop this plugin, you can check out the source code directly from the Bitbucket repository. Below are instructions for installing the plugin manually and getting it running.

##### Windows ######

- Place this folder in your %APPDATA%\Sublime Text 3\Packages folder. 
- Restart SublimeText. Any files that have a *.zs extension will automatically be opened as ZScript syntax.
- In your Package Control Settings, add the following line: ``"ignore_vcs_packages": true`` in order to prevent Package Control from freezing when trying to connect.


## Troubleshooting ##

All issues should be reported to the Bitbucket [issue tracker](https://bitbucket.org/sonictk/sublime_zscript/issues).


## Contributing ##

Feel free to fork this project and submit pull requests to fix issues described in the issue tracker. Recommended workflow is Git-Flow, simple style (feature branches, one main /master and /release branch.)


## Credits ##

Siew Yi Liang

Technical Artist at Frostburn Studios

admin@sonictk.com
